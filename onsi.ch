$TTL    86400 ; 24 hours could have been written as 24h or 1d
; $TTL used for all RRs without explicit TTL value
$ORIGIN onsi.ch.
@  1D  IN  SOA ns1.ondrejsika.ch. hostmaster.onndrejsika.ch. (
                  2002022405 ; serial
                  3H ; refresh
                  15 ; retry
                  1w ; expire
                  3h ; minimum
                 )
       IN  NS     ns1.ondrejsika.ch. ; in the domain
       IN  NS     ns2.ondrejsika.ch. ; external to domain
       IN  MX  10 mail.another.com. ; external mail provider
; server host definitions
ns1    IN  A      31.31.77.80  ;name server definition     
www    IN  A      31.31.77.80  ;web server definition
ftp    IN  CNAME  www.onsi.ch.  ;ftp server definition
; non server domain hosts
bill   IN  A      192.168.0.3
fred   IN  A      192.168.0.4 
